Objectifs :
Prendre le codage des images.
Prérequis à cette activité : 
Savoir les notions suivant : image, information et les conversions de binaire vers décimal et l’inverse.
Durée de l’activité : 2 heures.
Exercice ciblé : 
1.  Qu'est-ce que la "définition" pour une image ? Donnez des exemples 

2.  Qu'est-ce que  la  résolution ?  Donnez  les  valeurs  classiques  (écran, imprimante).

3.  Codage des couleurs :  Citez les différents types de codage en donnant des explications.

4.  Une image codée sur 65000 couleurs a une taille de 640 -  480 points.  Quelle est  sa taille physique en Koctets sur le disque dur ?

5.  Citez 6 extensions de fichiers image en donnant pour chaque extension :

     o  son type (couleur, noir et blanc ainsi que son codage),
     o  La compression si elle existe avec ses caractéristiques
     o  son utilisation
     o  d'autres informations complémentaires que vous avez envie de rajouter.

6.  Citez  5  logiciels  permettant  de  travailler  sur  les  images  en  spécifiant  (de  façon succincte)  quelles types d'opérations peuvent être réalisées.

7.  Vous réalisez un site internet. Quelles sont toutes les règles à prendre en compte pour utiliser les images ? A quoi correspondent ces codes ? FF0000 - 00FF00 - 0000FF
Description du déroulement de l’activité :
 • Donner aux apprenants cette activité. 
• Former des petits groupes.
 • Inviter les apprenants de chaque petit groupe à échanger leurs idées en précisant le temps qui leur est alloué.
 • Inviter chaque groupe à partager et à comparer leurs propos avec le groupe classe ou avec un autre petit groupe
. • Observer les apprenants et prendre des notes. 
• Faire un retour avec les apprenants sur l’efficacité des stratégies d’écoute et de prise de parole utilisées.
Anticipation des difficultés des élèves :
Le travail se veut totalement en groupe, parmi les difficultés des élèves c’est les conflits de groupe et aussi la manière de gérer le groupe.
Gestion de l’hétérogénéité :
1)	Faire des groupes de niveau
2)	Créer des binômes hétérogènes.
3)	Pratiquer des évaluations différenciées.
4)	Repérer les élèves intellectuellement précoces.
5)	La classe de soutien, de remise à niveau ou à rythme aménagé



