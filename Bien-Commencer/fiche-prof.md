Fiche de prof
Thématique : Les logiciels (traitement de texte).
Notions liées :  saisie, insertion d’objets.
Résumé de l’activité : Activité débranchée en deux parties. Traitement de texte insertion des images.
Objectifs : 
°	Définir les fonctionnalités de base du traitement de texte.
°	Connaitre et savoir utiliser les principaux outils de l’environnement du traitement de texte.
°	Savoir produire un document dans un traitement de text.
 
Auteur: EL BAZ Sanae
Durée de l’activité : 2h.
Forme de participation : en binôme, collective.
Matériel nécessaire : Papier, stylo, ordinateur.
Préparation : Aucune
Fiche Elève :                            
                              La Une d’un journal de presse désigne sa première page.
C'est généralement la seule qui sera visible avant l'achat du journal. Elle renseigne utilement sur le contenu du journal. Elle doit donc attirer l’œil du lecteur (l'abonné, le lecteur occasionnel, le passant dans la rue). La finalité de la Une est de faire vendre le journal. L’information y est organisée de façon à donner envie de lire le journal : elle doit être synthétique et attractive.

1)	Tapez le texte et l’organiser, Souligné le titre et encadre le texte ?
2)	Vérifiez l’orthographe et la grammaire du texte ?
3)	Prendre des images dans la vie quotidienne et le mettre ?
4)	Enregistre vos travaux ?


