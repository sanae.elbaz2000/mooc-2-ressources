Votre établissement produit chaque fin d’année un nouveau numéro de la revue d’établissement.
Vous faites partie de l’une des équipes de production et de rédaction de la revue. Cette équipe est chargée de réaliser des productions touchant les rubriques suivantes : Carte d’identité, thème de l’année.
Carte d’identité : le travail à réaliser
                                ° Une page sur l’établissement contenant :
                                ° Un titre.
                                ° Une photo récente de l’établissement (à scanner ou à l’aide d’un appareil photo numérique).
                                ° Un petit historique sur l’établissement.
Thème de l’année :
                  Travail à réaliser :
Choix logiciels : Traitement de texte.
Fiche descriptive du document à réaliser : thème
       Thèmes-exemples : Environnement, orientations.
Sommaire : 
      Définissez le plan de l’article.
Volume :
     Le nombre des pages estimé pour le travail.
Photos à insérer dans le document : 
     Cherchez les photos sur internet ou scannez des photos à partir d’encyclopédies appartenant à la bibliothèque de l’établissement.
      Citez les références des photos insérées.
